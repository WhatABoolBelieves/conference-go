import json
import requests
from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY


def request_photo(city, state):
    headers = {"Authorization": PEXELS_API_KEY}
    params = {
        "per_page": 1,
        "query": city + " " + state,
    }
    url = "https://api.pexels.com/v1/search"
    response = requests.get(url, params=params, headers=headers)
    content = response.json()
    try:
        return {"picture_url": content["photos"][0]["src"]["original"]}
    except:
        return {"picture_url": None}


def request_weather(city, state):
    headers = {"Authorization": OPEN_WEATHER_API_KEY}
    params = {
        "per_page": 1,
        "query": city + " " + state,
    }
    url = "http://api.openweathermap.org/geo/1.0/direct"
    response = requests.get(url, params=params, headers=headers)
    content = response.json()
    print(content)

    # Get the latitude and longitude from the response
    lat = content.lat
    lon = content.lon

    latLonUrl = (
        "https://api.openweathermap.org/data/3.0/onecall?lat="
        + lat
        + "&lon="
        + lon
        + "&appid="
        + OPEN_WEATHER_API_KEY
    )
    latLonResponse = requests.get(latLonUrl)
    latLonContent = latLonResponse.json()
    tempAndWeather = {
        "Temperature: ": latLonContent["main"]["temp"],
        "Current Weather: ": latLonContent["weather"][0]["description"],
    }
    return tempAndWeather
