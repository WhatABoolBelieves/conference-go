import json
import pika
from pika.exceptions import AMQPConnectionError
import django
import os
import sys
import time
from django.core.mail import send_mail


sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "presentation_mailer.settings")
django.setup()

while True:
    try:

        def process_approval(ch, method, properties, body):
            print("  Received %r" % body)

        parameters = pika.ConnectionParameters(host="rabbitmq")
        connection = pika.BlockingConnection(parameters)
        channel = connection.channel()
        channel.queue_declare(queue="tasks")
        channel.basic_consume(
            queue="tasks",
            on_message_callback=process_approval,
            auto_ack=True,
        )

        channel.start_consuming()
        presentation = json.dumps(body)
        send_mail(
            "Your presentation has been accepted",
            presentation.presenter_name
            + ", we're happy to tell you that your presentation "
            + presentation.title
            + " has been accepted",
            "admin@conference.go",
            ["presentation.presenter_email"],
            fail_silently=False,
        )
    except AMQPConnectionError:
        print("Could not connect to RabbitMQ")
        time.sleep(2.0)
